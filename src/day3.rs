use aoc_runner_derive::{aoc, aoc_generator};
use std::io::{BufRead, Cursor, Read};

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
struct Claim {
    id: String,
    x: u32,
    y: u32,
    width: u32,
    height: u32,
}

// example input: `#123 @ 3,2: 5x4`
fn parse_claim(input: &str) -> Claim {
    let mut input = Cursor::new(input.as_bytes());
    assert_eq!(input.read_until(b'#', &mut Vec::new()).unwrap(), 1);
    let mut buffer = Vec::new();
    input.read_until(b' ', &mut buffer).unwrap();
    let id = String::from_utf8(buffer.clone()).unwrap();
    assert_eq!(input.read_until(b'@', &mut Vec::new()).unwrap(), 2);
    assert_eq!(input.read_until(b' ', &mut Vec::new()).unwrap(), 1);
    buffer.clear();
    input.read_until(b',', &mut buffer).unwrap();
    let x = String::from_utf8_lossy(&buffer).parse().unwrap();
    buffer.clear();
    input.read_until(b':', &mut buffer).unwrap();
    let y = String::from_utf8_lossy(&buffer).parse().unwrap();
    assert_eq!(input.read_until(b' ', &mut Vec::new()).unwrap(), 1);
    buffer.clear();
    input.read_until(b'x', &mut buffer).unwrap();
    let width = String::from_utf8_lossy(&buffer).parse().unwrap();
    buffer.clear();
    input.read_to_end(&mut buffer).unwrap();
    let height = String::from_utf8_lossy(&buffer).parse().unwrap();
    Claim {
        id,
        x,
        y,
        width,
        height,
    }
}

#[aoc_generator(day3)]
fn parse(input: &str) -> Vec<Claim> {
    input.lines().map(parse_claim).collect()
}

#[aoc(day3, part1)]
fn part1(_input: &[Claim]) -> u32 {
    0
}
