use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::HashMap;

#[aoc_generator(day2)]
fn parse(input: &str) -> Vec<String> {
    input.lines().map(|s| s.to_owned()).collect()
}

#[aoc(day2, part1)]
fn part1(input: &[String]) -> u32 {
    let (doubles, triples) = input
        .iter()
        .map(|id| {
            let map = id.chars().fold(HashMap::new(), |mut map, c| {
                {
                    let count = map.entry(c).or_insert(0);
                    *count += 1;
                }
                map
            });
            let mut double = false;
            let mut triple = false;
            for v in map.values() {
                if !double && v == &2 {
                    double = true;
                }
                if !triple && v == &3 {
                    triple = true;
                }
                if triple && double {
                    break;
                }
            }
            (double, triple)
        })
        .fold((0, 0), |(d, t), kind| match kind {
            (true, true) => (d + 1, t + 1),
            (true, false) => (d + 1, t),
            (false, true) => (d, t + 1),
            _ => (d, t),
        });
    doubles * triples
}

fn diff_str(id1: &str, id2: &str) -> String {
    id1.chars()
        .zip(id2.chars())
        .filter(|(c1, c2)| c1 == c2)
        .map(|(c, _)| c)
        .collect()
}

#[aoc(day2, part2)]
fn part2(input: &[String]) -> String {
    for (i, id) in input.iter().enumerate() {
        for other in input.iter().skip(i + 1) {
            let diff = diff_str(id, other);
            if diff.len() == id.len() - 1 {
                return diff;
            }
        }
    }
    unreachable!();
}
