use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::HashSet;

#[aoc_generator(day1)]
fn parse_input(input: &str) -> Vec<i32> {
    input.lines().map(|l| l.parse().unwrap()).collect()
}

#[aoc(day1, part1)]
fn part1(input: &[i32]) -> i32 {
    input.iter().fold(0, |l, r| l + r)
}

#[aoc(day1, part2)]
fn part2(input: &[i32]) -> i32 {
    let mut set = HashSet::new();
    let mut freq = 0;
    set.insert(freq);
    for i in input.iter().cycle() {
        freq += i;
        if !set.insert(freq) {
            return freq;
        }
    }
    unreachable!();
}
